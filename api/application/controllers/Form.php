<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	class Form extends CI_Controller {
		public function index(){
			echo '¡Hola! Esto es un formulario de login.';
			$this->load->view('Form_view');
		}
		public function respuesta(){			
			//$this->load->library('input'); se comenta pq ya viene precargado en el framework			
			$login = $this->input->post('login');
			$passwd = $this->input->post('login');				

			if($login && $passwd){
				$data['login'] = $_REQUEST['login'];
				$data['passwd'] = $_REQUEST['passwd'];		
				$this->load->view('Res_view', $data);
			}else{
				?>
					<script>alert('Introduce tus datos.');</script>
				<?php
				$this->parser->parse('Form_view');
			}	
		}
		public function usuario($usuario = NULL){			
			$login = $this->input->post('login');
			$passwd = $this->input->post('passwd');
			//recojo los datos para poder consultar la bbdd
			$this->load->model('User_model');
			$usuario2 = $this->User_model->get_usuario($login,$passwd);
			//aqui llamar a una vista pasando los usuarios
			foreach($usuario2 as $u){
				echo $u['id'].' ';
				echo $u['login'].' ';
				echo $u['password'].'<br>';				
			}			
		}
		public function all(){
			/*$this->load->model('User_model');
			$resultado = $this->User_model->get_usuarios();
			foreach($resultado as $u){
				echo $u['id'].' ';
				echo $u['login'].' ';
				echo $u['password'].'<br>';
				echo '<style>body{font-size:75px;font-family:arial;background-color:'.$u['color'].'}</style>';
			}	*/

			$this->load->model('User_model');
			$resultado = $this->User_model->get_usuarios();
			$data['usuario']=$resultado;
			$this->parser->parse('all_users',$data);

		}
	}
?>
