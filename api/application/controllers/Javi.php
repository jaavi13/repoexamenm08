<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Javi extends CI_Controller {
	public function index()
	{
		echo 'Hola! Soy Javi.';
	}
	public function polla()
	{
		$this->load->model('Javi_model');
		$nombre = $this->Javi_model->nombre();
		$data['page_title'] = 'Javi Web';
		$data['login'] = $nombre;
		$this->load->view('javiview',$data);		
	}
	public function pollapolla($polla='')
	{
		echo 'Esto es la polla.'.$polla;
	}
}
