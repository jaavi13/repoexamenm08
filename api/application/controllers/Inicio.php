<?php	
	defined('BASEPATH') OR exit('No direct script access allowed');
	class Inicio extends CI_Controller {
		public function index(){					
			$this->load->view('Inicio_view');
		}
		public function get(){
			$this->load->model('Excel_model');
			$resultado = $this->Excel_model->get_all();
			$data = array('resultado'=>$resultado);
			$this->load->view("Get_view",$data);
		}
		public function post(){
			$this->load->model('Excel_model');
			$reference = $this->input->post("reference");
			$artist = $this->input->post("artist");
			$resultado = $this->Excel_model->envioDatos($reference,$artist);
		}
		public function put(){
			$this->load->model('Excel_model');
			$resultado = $this->Excel_model->get_all();
			$data = array('resultado'=>$resultado);			
			$reference = $this->input->post("reference");
			$artist = $this->input->post("artista");
			$this->Excel_model->modificar($reference, $artist);
			$this->load->view("Put_view",$data);
		}
		public function del(){
			$this->load->model('Excel_model');
			$resultado = $this->Excel_model->get_all();
			$data = array('resultado'=>$resultado);
			$reference = $this->input->post("reference");
			$this->Excel_model->eliminar($reference);
			$this->load->view("Del_view",$data);			
		}
	}	
?>
