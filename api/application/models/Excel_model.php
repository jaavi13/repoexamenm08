<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	class Excel_model extends CI_Model {
		public function get_all(){//coge todos los usuarios
			/*$query = $this->db->get('comentarios');
			return $query->result_array();*/
			$url='https://concert.apispark.net:443/v1/hoja_1s/';
			//  Initiate curl
			$ch = curl_init();
			// Disable SSL verification
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			// Will return the response, if false it print the response
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			// Set the url
			curl_setopt($ch, CURLOPT_URL,$url);
			// Execute
			$result=curl_exec($ch);
			// Closing
			curl_close($ch);
			
			return json_decode($result, true);
		}
		public function envioDatos($reference, $artist){	
			$data = array("reference" => $reference, "artist" => $artist) ;                                                              
			$data_string = json_encode($data);                                                                                   		
			var_dump($data_string)	;                                                                                                                    
			$ch = curl_init('https://concert.apispark.net:443/v1/hoja_1s/');                                                   
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");   
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			// Will return the response, if false it print the response
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                  
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
			    'Content-Type: application/json',                                                                                
			    'Content-Length: ' . strlen($data_string))                                                                       
			);                                                                                                                   
			                                                                                                                     
			$result = curl_exec($ch);
		}
		public function modificar($reference,$artist){ 
			$data = array("reference" => $reference, "artist" => $artist);
			$data_string = json_encode($data);
			//var_dump($data_string);
			$curl = curl_init();         	
			curl_setopt_array($curl, array(
			  CURLOPT_URL => "https://concert.apispark.net/v1/hoja_1s/".$reference."",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "PUT",
			  CURLOPT_SSL_VERIFYPEER => false,
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_POSTFIELDS => $data_string,
			  CURLOPT_HTTPHEADER => array(
			    "accept: application/json",
			    "authorization: Basic NzZhYTUxMDUtM2I5NS00MGJmLWEzOGUtYzBlOWE3M2UxNmIwOjU1NjM4NWM2LTc2MjUtNGM0ZC1hM2UxLTkyM2UwZmQ2ZGM4MQ==",
			    "content-type: application/json",
			    "host: concert.apispark.net"
			  ),
			));

			$response = curl_exec($curl);
			$err = curl_error($curl);

			curl_close($curl);

			if ($err) {
			  echo "cURL Error #:" . $err;
			} else {
			  echo $response;
			}
		}

		function Redirect($url, $permanent = false)
		{
		    if (headers_sent() === false)
		    {
		        header('Location: ' . $url, true, ($permanent === true) ? 301 : 302);
		    }

		    exit();
		}

		public function eliminar($reference){ 
			$curl = curl_init();
			echo "https://concert.apispark.net/v1/hoja_1s/".$reference."";
			curl_setopt_array($curl, array(
			  CURLOPT_URL => "https://concert.apispark.net/v1/hoja_1s/".$reference."",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "DELETE",
			  CURLOPT_SSL_VERIFYPEER => false,
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_HTTPHEADER => array(
			    "accept: application/json",
			    "authorization: Basic NzZhYTUxMDUtM2I5NS00MGJmLWEzOGUtYzBlOWE3M2UxNmIwOjU1NjM4NWM2LTc2MjUtNGM0ZC1hM2UxLTkyM2UwZmQ2ZGM4MQ==",
			    "content-type: application/json",
			    "host: concert.apispark.net"
			  ),
			));

			$response = curl_exec($curl);
			$err = curl_error($curl);

			curl_close($curl);

			if ($err) {
			  echo "cURL Error #:" . $err;
			} else {
			  echo $response;
			}
		}
}