<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	class Coment_model extends CI_Model {
		public function get_coments(){//coge todos los usuarios
			$query = $this->db->get('comentarios');
			return $query->result_array();
		}
		public function consulta($id){
			$query = $this->db->get_where('comentarios', array('id'=>$id));
			return $query->result_array();
		}
		public function insert_coments($comentario){
			$data = array(
					'valor' => $comentario
				);
			$this->db->insert('comentarios',$data);
		}
		public function update_coments($id, $comentario){
			$datos = array(
						'valor' => $comentario
				);
			$this->db->where('id',$id);
			$this->db->update('comentarios',$datos);
		}
	}
?>
