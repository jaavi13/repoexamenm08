<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	class User_model2 extends CI_Model {
		public function get_usuarios(){//coge todos los usuarios
			$query = $this->db->get('usuarios');
			return $query->result_array();
		}
		public function get_usuario($usuario,$contrasenya){//coge un usuario y comprueba su login y contraseña		
			$query = $this->db->get_where('usuarios',array('login'=>$usuario,'password'=>$contrasenya));	
			$resultado = $query->result_array();	
						
			return $resultado;
		}
		public function addUser($login, $password, $rol){
			$data = array(
					'login' => $login,
					'password' => $password,
					'rol' => $rol
				);
			$this->db->insert('usuarios',$data);
		}
		public function delUser($login){
			$this->db->delete('usuarios', array('login' => $login));
		}
	}
?>
