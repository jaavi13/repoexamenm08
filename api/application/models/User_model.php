<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	class User_model extends CI_Controller {
		/*public function __construct(){
			//$this->load->database(); ya está precargada
		}*/
		public function get_usuarios(){
			//$this->db->get('usuarios'); esto es lo mismo que lo de abajo
			//$query = $this->db->query('SELECT * FROM usuarios');
			$query = $this->db->get('usuarios');
			return $query->result_array();//recoge los valores de la consulta
		}

		public function get_usuario($usuario,$contrasenya){
			//$this->db->get('usuarios'); esto es lo mismo que lo de abajo
			//$query = $this->db->query("SELECT * FROM usuarios WHERE login ='".$usuario."'");
			$query = $this->db->get_where('usuarios',array('login'=>$usuario));
			$query2 = $this->db->get_where('usuarios',array('password'=>$contrasenya));
			$resultado = $query->result_array();
			$resultado2 = $query2->result_array();		

			if(!$resultado && !$resultado2){
				echo '<script>alert("No existe éste usuario."); history.back();</script>';
			}else if(!$resultado){
				echo '<script>alert("El nombre es incorrecto."); history.back();</script>';
			}else if(!$resultado2){
				echo '<script>alert("La contraseña es incorrecta."); history.back();</script>';
			}			
			return $resultado;//recoge los valores de la consulta
		}
	}
?>
