<html>
<head>
        <style>*{font-family: sans-serif;margin-top: 8%;margin-left: 17%;}</style>
        <title>Examen Javier Quintanar</title>
        <!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>
<body>
	<h1>Examen Javier Quintanar</h1>
	<form action="<?php echo base_url('Inicio/get')?>">	
       <input class="btn btn-primary" type="submit" value="GET">       		
	</form>

	<form action="<?php echo base_url('Inicio/post')?>" method="POST">
		Reference: <input class="input-block-level" type="text" name="reference">
		Artist: <input class="input-block-level" type="text" name="artist">
		<input class="btn btn-success" type="submit" value="POST">
	</form>

	<form action="<?php echo base_url('Inicio/put')?>" method="POST">		
		<input class="btn btn-warning" type="submit" value="PUT">
	</form>

	<form action="<?php echo base_url('Inicio/del')?>" method="POST">		
		<input class="btn btn-danger" type="submit" value="DELETE">
	</form>
</body>
</html>